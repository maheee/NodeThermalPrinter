NodeThermalPrinter
==================

## Description

If you have a printer like [this](http://files.terraex.de/thermalprinter/20140310_221551.jpg) you can print stuff like [this](http://files.terraex.de/thermalprinter/20140310_221448.jpg).

## Usage

See test.js.

## Dependencies

### For the driver only:

* https://github.com/devongovett/png.js/
* https://github.com/caolan/async/

```
    npm install png-js
    npm install async
```

png-js is only needed for png2thermal.js and printing images.
async is not needed for the library but only for test.js.

### For the test webapp:

* http://expressjs.com/

```
    npm install express
    npm install body-parser
    npm install json-middleware
```

