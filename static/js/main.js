
$('#print_button').click(function () {
  var text = $('#print_text').val();
  var lines = text.split('\n');

  var printRequest = {};
  printRequest.messages = [];

  var i;
  for (i in lines) {
    printRequest.messages.push({'text': lines[i]});
  }
  console.log(printRequest);

  $.post('./print', printRequest);
});

