/*
 * Looks like it only works with color PNGs, maybe grayscale as well.
 * It fails miserably with indexed ones!
 */
const PNG = require('png-js');

const FG_COLOR = 1;
const BG_COLOR = 0;

//TODO this sucks and needs to be configurable
function color2bw(r, g, b, a) {
  if (a < 200) {
    return BG_COLOR;
  }

  c = (r + g + b) / 3;
  if (c > 200) {
    return BG_COLOR;
  }

  return FG_COLOR;
}

function png2bwArray(filename, callback) {
  var image = new PNG.load(filename);
  
  var widthFill = 8 - (image.width % 8);
  var heightFill = 8 - (image.height % 8);

  if (widthFill == 8) widthFill = 0;
  if (heightFill == 8) heightFill = 0;

  var imgLines = [];

  image.decode(function(pixels) {
    var i, p, t;
    var line = [];

    for (i = 0; i < pixels.length; i += 4) { // always add 4 for red, blue, green and alpha
      if (i % (image.width * 4) == 0 && line.length > 0) {
        // fill line to multiple of 8
        for (j = 0; j < widthFill; j++) {
          line.push(BG_COLOR);
        }
        imgLines.push(line);
        line = [];
      }
      line.push(color2bw(pixels[i], pixels[i+1], pixels[i+2], pixels[i+3]));
    }

    // fill with empty lines to have multiple of 8
    for (i = 0; i <= heightFill; i++) {
      line = [];
      for (j = 0; j < image.width+widthFill; j++) {
        line.push(BG_COLOR);
      }
      imgLines.push(line);
    }

    callback(imgLines);
  });

}

function valueFromPixelArrayPos(pixel, x, y) {
  var value = 0;
  value |= (pixel[y  ][x]   ? 128 : 0 );
  value |= (pixel[y+1][x]   ?  64 : 0 );
  value |= (pixel[y+2][x]   ?  32 : 0 );
  value |= (pixel[y+3][x]   ?  16 : 0 );
  value |= (pixel[y+4][x]   ?   8 : 0 );
  value |= (pixel[y+5][x]   ?   4 : 0 );
  value |= (pixel[y+6][x]   ?   2 : 0 );
  value |= (pixel[y+7][x]   ?   1 : 0 );
  return value;
}

function pixelArray2thermal(pixel) {
  var result = [];
  var resultLine;
  var x, y, value;

  for (y = 0; y < pixel.length; y += 8) {
    resultLine = "";
    for (x = 0; x < pixel[y].length; x++) { // if its not a perfect rectangle, we are screwed anyway
      resultLine += String.fromCharCode(
        valueFromPixelArrayPos(pixel, x, y)
      );
    }
    result.push(resultLine);
  }

  return result;
}

function png2thermal(filename, callback) {
  png2bwArray(filename, function (pixel) {
    var w = pixel[0].length;
    var h = pixel.length;
    callback(w/8, h/8, pixelArray2thermal(pixel));
  });
}

exports.png2thermal = pixelArray2thermal;
exports.png2thermal = png2thermal;
