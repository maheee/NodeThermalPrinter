const fs = require('fs');
const async = require('async');
const Printer = require('./thermal_printer_lib').Printer;
const png2thermal = require('./png2thermal').png2thermal;

const FILE = '/dev/ttyO4';
const ENC = 'ascii';

var stream = fs.createWriteStream(FILE, {encoding: ENC});

var p = new Printer(function (line) {
  stream.write(line, ENC);
});

function init(p) {
  p.init();
  p.setUserDefChar(0);
  p.selectIntCharSet( p.C_GERMANY );
}

async.series([
  function (cb) {init(p); cb();},
  function (cb) { setTimeout(cb, 500); },
  function (cb) {
    p.b("Test print started!").lf();

    p.setUpdownMode(1);
    p.b("Upside down ...").lf();
    p.setUpdownMode(0);

    p.setInverseMode(1);
    p.b("Inverse ...").lf();
    p.setInverseMode(0);

    p.setAlign(p.CENTER);
    p.b("... centered ...").lf();
    p.setAlign(p.RIGHT);
    p.b("... right").lf();
    p.setAlign(p.LEFT);

    cb();
  },
  function (cb) { setTimeout(cb, 500); },
  function (cb) {
    p.setPrintMode(true);
    p.b("Soup Barcode:").lf().lf();

    p.setAlign(p.CENTER);
    p.setBarcodeWidth(3);
    p.setBarcodeTextPos(p.BT_BELOW);
    p.printBarcode(p.BC_EAN13, "9000275651717");

    cb();
  },
  function (cb) { setTimeout(cb, 500); },
  function (cb) {
    p.setAlign(p.LEFT);
    p.b("QR-Code:").lf().lf();

    png2thermal('./qr.png', function (w, h, lines) {
      var i, pos;
      i = 0;
      for (i in lines) {
        p.defineImage(w, 1, lines[i]);
        p.printImage(p.I_NORMAL);
        //p.printImage(p.I_QUADRUPLE);
      }
      cb();
    });
  },
  function (cb) { setTimeout(cb, 500); },
  function (cb) {
    p.lf().lf();

    cb();
  },
], function (err, res) {
  console.log(err);
  console.log(res);
});
