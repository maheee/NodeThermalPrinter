
//Helper
const ASCII = {
  LF  : '\x0A',
  S0  : '\x0E',
  DC4 : '\x14',
  ESC : '\x1B',
  GS  : '\x1D',
  AT  : '\x40',
};

function isString(obj) {
  return Object.prototype.toString.call(obj) == '[object String]';
}

function createOutFunction(line) {
  return function (line2) {
    var resLine = line;
    if (line2 != undefined) {
      resLine += (isString(line2) ? line2 : String.fromCharCode(line2));
    }
    this.w(resLine);
    return this;
  };
};

//Constructor
var Printer = function (writer) {
  this.w = writer;
};

//Constants
Printer.prototype.C_USA           = 0;
Printer.prototype.C_FRANCE        = 1;
Printer.prototype.C_GERMANY       = 2;
Printer.prototype.C_UK            = 3;
Printer.prototype.C_DENMARK_1     = 4;
Printer.prototype.C_SWEDEN        = 5;
Printer.prototype.C_ITALY         = 6;
Printer.prototype.C_SPAIN_1       = 7;
Printer.prototype.C_JAPAN         = 8;
Printer.prototype.C_NORWAY        = 9;
Printer.prototype.C_DENMARK_2     = 10;
Printer.prototype.C_SPAIN_2       = 11;
Printer.prototype.C_LATIN_AMERICA = 12;
Printer.prototype.C_KOREA         = 13;

Printer.prototype.P_437 = 0;
Printer.prototype.P_850 = 1;

Printer.prototype.BC_UPCA    =  0;
Printer.prototype.BC_UPCE    =  1;
Printer.prototype.BC_EAN13   =  2;
Printer.prototype.BC_EAN8    =  3;
Printer.prototype.BC_CODE39  =  4;
Printer.prototype.BC_I25     =  5;
Printer.prototype.BC_CODEBAR =  6;
Printer.prototype.BC_CODE93  =  7;
Printer.prototype.BC_CODE128 =  8;
Printer.prototype.BC_CODE11  =  9;
Printer.prototype.BC_MSI     = 10;

Printer.prototype.BT_HIDDEN = 0;
Printer.prototype.BT_ABOVE  = 1;
Printer.prototype.BT_BELOW  = 2;
Printer.prototype.BT_BOTH   = 3;

Printer.prototype.I_NORMAL        = 0;
Printer.prototype.I_DOUBLE_WIDTH  = 1;
Printer.prototype.I_DOUBLE_HEIGHT = 2;
Printer.prototype.I_QUADRUPLE     = 3;

Printer.prototype.LEFT   = 0;
Printer.prototype.CENTER = 1;
Printer.prototype.RIGHT  = 2;

Printer.prototype.DISABLE = 0;
Printer.prototype.ENABLE  = 1;

//Print Commands
Printer.prototype.b = createOutFunction('');
Printer.prototype.lf = createOutFunction('\n');
Printer.prototype.printAndFeed = createOutFunction(ASCII.ESC + 'J'); // 0-255 dots

//Line Spacing Setting Commands
Printer.prototype.defaultLineSpacing = createOutFunction(ASCII.ESC + '2');
Printer.prototype.setLineSpacing = createOutFunction(ASCII.ESC + '3'); // 0-255 dots (default is 30 ?)
Printer.prototype.setAlign  = createOutFunction(ASCII.ESC + 'a'); // LEFT/CENTER/RIGHT
Printer.prototype.setIndent = createOutFunction(ASCII.ESC + 'B'); // 0-47

//Character Commands
Printer.prototype.setPrintMode = function (tiny, emphasize, doubleHeight, doubleWidth, underline) {
  var mode = 0;
  mode |= ( tiny         ?   1 : 0 );
  //mode |= ( ???          ?   2 : 0 );
  //mode |= ( ???          ?   4 : 0 );
  mode |= ( emphasize    ?   8 : 0 );
  mode |= ( doubleHeight ?  16 : 0 );
  mode |= ( doubleWidth  ?  32 : 0 );
  //mode |= ( deleteLine   ?  64 : 0 );
  mode |= ( underline    ? 128 : 0 );
  this.w(ASCII.ESC + '!' + String.fromCharCode(mode));
  return this;
};
Printer.prototype.enableDoubleWidthMode  = createOutFunction(ASCII.ESC + ASCII.S0 );
Printer.prototype.disableDoubleWidthMode = createOutFunction(ASCII.ESC + ASCII.DC4);
Printer.prototype.setUpdownMode  = createOutFunction(ASCII.ESC + '{'); // ENABLE/DISABLE
Printer.prototype.setInverseMode = createOutFunction(ASCII.GS  + 'B'); // ENABLE/DISABLE
Printer.prototype.setUserDefChar = createOutFunction(ASCII.ESC + '%'); // ENABLE/DISABLE
Printer.prototype.selectIntCharSet    = createOutFunction(ASCII.ESC + 'R'); // C_*
Printer.prototype.selectCharCodeTable = createOutFunction(ASCII.ESC + 't'); // P_*

//Bit Image Commands
Printer.prototype.defineImage = function (width, height, data) {
  if (data.length != width * height * 8) {
    throw "Invalid data length!";
  }
  this.w(ASCII.GS + '*' + String.fromCharCode(width) + String.fromCharCode(height) + data);
  return this;
};
Printer.prototype.printImage = createOutFunction(ASCII.GS + '/'); // I_*

//Init Commands
Printer.prototype.init = createOutFunction(ASCII.ESC + ASCII.AT);

//Bar Code Commands
Printer.prototype.setBarcodeTextPos = createOutFunction(ASCII.GS + 'H'); // BT_*
Printer.prototype.setBarcodeHeight  = createOutFunction(ASCII.GS + 'h'); // 1-255 (default is 50 ?)
Printer.prototype.setBarcodeWidth   = createOutFunction(ASCII.GS + 'w'); // 2,3 (default is 2 ?)
Printer.prototype.printBarcode = function (type, line) {
  this.w(ASCII.GS + 'k' + String.fromCharCode(type) + line + String.fromCharCode(0));
  return this;
};

exports.Printer = Printer;
