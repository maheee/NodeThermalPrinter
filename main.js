//printer
const fs = require('fs');
const async = require('async');
const Printer = require('./thermal_printer_lib').Printer;
const png2thermal = require('./png2thermal').png2thermal;

const FILE = '/dev/ttyO4';
const ENC = 'ascii';

//express
const express = require('express');
const bodyParser = require('body-parser');

//init printer
var stream = fs.createWriteStream(FILE, {encoding: ENC});

var p = new Printer(function (line) {
  stream.write(line, ENC);
});

function init(p) {
  p.init();
  p.setUserDefChar(0);
  p.selectIntCharSet( p.C_GERMANY );
}

function printLine(msg) {
  console.log(msg['text']);
  p.b(msg['text']).lf();
}

function print(messages, response) {
  async.series([
    function (cb) {init(p); cb();},
    function (cb) { setTimeout(cb, 500); },
    function (cb) {
      for (i in messages) {
        printLine(messages[i]);
      }
      p.lf().lf();
      cb();
    },
    function (cb) {
      response.send("OK");
      cb();
    },
  ], function (err, res) {
    if (err) {
      response.send("ERROR");
      console.log(err);
    }
  });

}

//init app
var app = express();
app.use(bodyParser());
app.use(express.static(__dirname + '/static'));


app.post('/print', function (req, res) {
  if (req && req.body && req.body.messages) {
    print(req.body.messages, res);
  }
});

app.get('/', function (req, res) {
  res.send("Hello World!");
});


var server = app.listen(8084, 'localhost', function() {
  console.log("Listening on port %d", server.address().port);
});

